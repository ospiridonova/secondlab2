package services;

import org.apache.log4j.Logger;
import org.apache.log4j.extras.DOMConfigurator;

import java.sql.*;

/**
 * Created by Olesya on 13.04.2017.
 */

/**
 * Класс предназначен для соединения и выполнения операций над базой данных
 */
public class DatabaseManager {

    private static Connection connection;

    private static Logger logger = Logger.getLogger(DatabaseManager.class);

    static {
        DOMConfigurator.configure("log4j.xml");
    }

    /**
     * Метод для инициализации подключения к базе данных
     */
    public static void initConnection() {

        try {
            if (connection != null) connection.close();
            Class.forName("org.postgresql.Driver");
            connection =
                    DriverManager.getConnection(
                            "jdbc:postgresql://localhost/inno_crm",
                            "inno_crm", "inno_crm");

            logger.trace("Подключен к БД");
            System.out.println("Ok");
        } catch (ClassNotFoundException e) {
            logger.error(e);
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для получения информации из таблицы Студенты
     */
    public void selectStudent() {
        try {
            initConnection();
            Statement statement = connection.createStatement();
            ResultSet result =
                    statement.executeQuery("select * from public.student");
            while (result.next()) {
                System.out.println(result.getString(1));
                System.out.println(result.getString(2));
                System.out.println(result.getString(3));
                System.out.println(result.getString(4));
                System.out.println(result.getString(5));
                System.out.println(result.getInt(6));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }

    }

    /**
     * Метод для заполнения таблицы Студенты
     */
    public void insertStudent() {
        try {
            initConnection();
            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO student( " +
                            "    std_surname, std_name, std_patronymic, std_email, std_rating) " +
                            "    VALUES ( ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, "Ivanov");
            preparedStatement.setString(2, "Sidor");
            preparedStatement.setString(3, "Sidorovich");
            preparedStatement.setString(4, "sidor@gmail.com");
            preparedStatement.setInt(5, 100);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }


}
